# Stack de Monitoramento Docker + Zabbix

## Base zabbix 6.4 - MySQL - Alpine:

zabbix-server-mysql
zabbix-web-nginx-mysql
zabbix-agent
zabbix-java-gateway
mysql-server

1. Para obter os arquivos, clone o repositório:

```
git clone https://gitlab.com/dockermon/docker-zabbix.git
```

2. Defina as passwords do MySQL nos arquivos:

```
env_vars/.MYSQL_PASSWORD
env_vars/.MYSQL_ROOT_PASSWORD
```

3. Defina as informações do Let's Encrypt no docker-compose.yml:

```
   - VIRTUAL_HOST=mon.seu_dominio.com
   - LETSENCRYPT_HOST=mon.seu_dominio.com
   - LETSENCRYPT_EMAIL=seu_email@seu_dominio.com
```

* Se desejar, faça os ajustes necessários nos arquivos de variáveis dentro de "env_vars".

4. Inicie a stack:

```
docker-compose up -d
```

